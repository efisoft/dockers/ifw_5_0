FROM fedora:34
# Objectiv
# Install a Fedora 34
# With Elt Devenv 4.8.0
# with IFW 5.0

#To see available ESO distribution check into https://ftp.eso.org/pub/elt/repos/fedora/eso/<FedoraVersion>
# When writing this file
# ****** FEDORA 34 ****** 
# elt-4.1.0 --> elt-4.6.0
# ****** FEDORA 38 ****** 
# elt-5.1.0
# elt-beta
# 
# N.B ESO doc is based on Fedora:24 with no parameter 
 
ARG ELT_DIST=elt-4.8.0
ENV ELT_DIST=${ELT_DIST}
 
# ELT_ROLE determines what will be installed by puppet
# Select ELTDEV or MINIMAL
ARG ELT_ROLE=ELTDEV
ENV ELT_ROLE=${ELT_ROLE}
 
# Delegate OpenGL rendering to host X server
ENV LIBGL_ALWAYS_INDIRECT=1
  
# install wget
run dnf install -y wget

# install pacth
run dnf install -y patch

# install selinux components
#run dnf install -y selinux-policy


#install selinux
#run yum -y install selinux-policy


# ESO-287339_ELT_Linux_Installation_Guide_7.2 - 4.5 Installation of puppet packages 
# RUN sh <(curl https://ftp.eso.org/pub/elt/repos/fedora/eso/GetDevEnv.sh)
RUN sh <(curl https://ftp.eso.org/pub/elt/repos/fedora/eso/GetDevEnv.sh) $ELT_DIST  


    
# Fixup a few things for it to work withing docker
COPY ./patches /root/patches
RUN echo GRUB_CMDLINE_LINUX="mitigations=off" > /etc/default/grub
#virt-what need priviledged access to run. We shunt it and force a "docker" output
#no more needed
#RUN echo "echo docker" > /usr/sbin/virt-what

# Fixup to avoid installation error due to init.d being directory
RUN touch /etc/init.d
 
#Install missing package 
RUN yum -y install hostname

  
# Patching dev puppet config
RUN patch /root/elt/puppet/modules/ESO/eltdev/manifests/init.pp /root/patches/eltdev/init.pp.patch
RUN patch /root/elt/puppet/modules/ESO/eltdev/manifests/sysrpms.pp /root/patches/eltdev/sysrpms.pp.patch
   
   
#################### SYSTEMCTL Docker compatibility issue correction ###############################   
# Replace systemctl with docker-compatible replacement
RUN wget https://raw.githubusercontent.com/gdraheim/docker-systemctl-replacement/master/files/docker/systemctl3.py -O /usr/bin/systemctl
 
RUN  chgrp systemd-journal /var/log/journal && \
     chmod 777 /var/log/journal/
     
RUN patch /usr/bin/systemctl /root/patches/eltdev/systemctl.py.patch

ENV container=docker
##################################################################################################   

# Enable IP V6
#RUN echo 'net.ipv6.conf.all.disable_ipv6 = 0' >> /etc/sysctl.d/99-sysctl.conf
#RUN echo 'net.ipv6.conf.default.disable_ipv6 = 0' >> /etc/sysctl.d/99-sysctl.conf


# Running puppet to sync ELT deps
WORKDIR /root/elt
 
# we actually install ELT DevEnv
# We allow first run of puppet to fail
RUN FAST_YUM_INSTALL=YES ./puppet-force-align > ~/PuppetForceAligne01.txt; exit 0
RUN FAST_YUM_INSTALL=YES ./puppet-force-align > ~/PuppetForceAligne02.txt; exit 0
RUN FAST_YUM_INSTALL=YES ./puppet-force-align > ~/PuppetForceAligne03.txt; exit 0
RUN FAST_YUM_INSTALL=YES ./puppet-force-align > ~/PuppetForceAligne04.txt; 
RUN ./puppet-check > ~/PuppetCheckResult.txt
 
#patching selinux
RUN patch  /etc/selinux/config /root/patches/selinux/config.patch
 

#installing CII services
# Patching base puppet config
RUN patch -l /elt/ciisrv/postinstall/puppet/modules/schemas/manifests/init.pp /root/patches/ciisrv/schemas/init.pp.patch
RUN patch -l /elt/ciisrv/postinstall/puppet/modules/calculation_node_service/manifests/init.pp /root/patches/ciisrv/calculation_node_service/init.pp.patch
RUN patch -l /elt/ciisrv/postinstall/puppet/modules/calculation_scheduler_service/manifests/init.pp /root/patches/ciisrv/calculation_scheduler_service/init.pp.patch
RUN patch -l /elt/ciisrv/postinstall/puppet/modules/elasticsearch/manifests/init.pp /root/patches/ciisrv/elasticsearch/init.pp.patch
# RUN patch -l /elt/ciisrv/postinstall/puppet/modules/filebeat/manifests/init.pp /root/patches/ciisrv/filebeat/init.pp.patch
RUN patch -l /elt/ciisrv/postinstall/puppet/modules/kibana/manifests/init.pp /root/patches/ciisrv/kibana/init.pp.patch
RUN patch -l /elt/ciisrv/postinstall/puppet/modules/minio/manifests/init.pp /root/patches/ciisrv/minio/init.pp.patch
RUN cat  /root/patches/ciisrv/serverlocation/init.pp > /elt/ciisrv/postinstall/puppet/modules/serverlocation/manifests/init.pp 

# ELT CII Services
# N.B. : Return code is 2, with no error. We force 0
# cii-post install we fail due to impossibility to modify /etc/hosts
# needed entries are added using --add-host selector during build
RUN /elt/ciisrv/postinstall/cii-postinstall  role_ownserver; exit 0
#Running all bring memory issue. Its linked to logLogstash
#we replace start all by individual launch to avoid this issue
# see https://ftp.eso.org/pub/elt/repos/docs/HLCC/webpages/hlcc-main/html/html/ chap.4 for potential known memory issues
RUN cii-services start all ; exit 0
 
# 2 - Install IFW components
RUN  yum -y install elt-ifw-5.0.0


# 3 - Install HLCC Components
RUN yum -y install elt-hlcc-sw-devel-1.2.0 elt-hlcc-if-devel-1.2.0


RUN echo "root:root" | chpasswd

#need for hlccOldbloader to be able to create logs
# TODO give only R/W access
#RUN chmod -R 777 /var/log/
RUN setfacl -b -R /var/log
RUN chmod -R g=rwx /var/log/journal

# for easy acces install nautilus file manager
RUN yum -y install nautilus

RUN cii-services stop all ; exit 0
# 4 - Login as eltdev user.
USER eltdev 

# 5 - Create HLCC database structure
ENV PYTHONPATH=/elt/wtools/lib/python3.9/site-packages:/elt/seq/lib/python3.9/site-packages:/elt/rad/lib64/python3.9/site-packages:/elt/rad/lib/python3.9/site-packages:/elt/oldbloader/lib/python3.9/site-packages:/elt/msgsend/lib/python3.9/site-packages:/elt/mal/lib/python3.9/site-packages:/elt/lsv/lib/python3.9/site-packages:/elt/ifw/lib/python3.9/site-packages:/elt/etr/lib/python3.9/site-packages:/elt/trs/lib/python3.9/site-packages:/elt/ecs-interfaces/lib/python3.9/site-packages:/elt/ddt/lib/python3.9/site-packages:/elt/cut/lib64/python3.9/site-packages:/elt/cut/lib/python3.9/site-packages:/elt/ciisrv/lib64/python3.9/site-packages:/elt/ciisrv/lib/python3.9/site-packages:/elt/hlcc/lib/python3.9/site-packages:/elt/rad/share/waftools
ENV PATH=/home_local/eltdev/.local/bin:/home_local/eltdev/bin:/home_local/eltdev/bin:/home_local/eltdev/.local/bin:/elt/seq/bin:/elt/rad/bin:/elt/oldbloader/bin:/elt/msgsend/bin:/elt/mal/bin:/elt/lsv/bin:/elt/ifw/bin:/elt/etr/bin:/elt/trs/bin:/elt/ecs-interfaces/bin:/elt/ddt/bin:/elt/cut/bin:/elt/ciisrv/bin:/elt/common/bin:/home_local/eltdev/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/elt/hlcc/bin:/usr/share/elasticsearch/bin
ENV LD_LIBRARY_PATH=/elt/taiclock/lib64:/elt/seq/lib64:/elt/rtms/lib64:/elt/rad/lib64:/elt/mudpi/lib64:/elt/mal/lib64:/elt/lsv/lib64:/elt/ifw/lib64/designer:/elt/ifw/lib64:/elt/trs/lib64:/elt/ecs-interfaces/lib64:/elt/ddt/lib64:/elt/cut/lib64:/elt/ciisrv/lib64:/elt/common/lib:/elt/common/lib64:/elt/hlcc/lib64
ENV CFGPATH=/elt/rad/resource:/elt/lsv/resource:/elt/ifw/resource:/elt/hlcc/resource:/elt/hlcc
ENV CII_LOGS=/var/log/elt
#Create HLCC database structure
#hlccinstall needs services to be started. Embed into the same docker layer
RUN sudo /elt/ciisrv/bin/cii-services start all && /elt/ciisrv/bin//cii-services status && /elt/hlcc/bin/hlccOldbloader --deployment ins



# 6 - Create the directories for the installation areas:
ENV COOKIECUTTER_CONFIG=/elt/common/templates/cookiecutter/elt-cookiecutter-config.yaml
ENV SYS_ROOTDIR=/elt
ENV SYS_TEMPLATES=/elt/common/templates
WORKDIR /home_local/eltdev
RUN /elt/common/bin/getTemplate -d introot INTROOT 
WORKDIR /home_local/eltdev
RUN /elt/common/bin/getTemplate -d dataroot DATAROOT

# 7 - Under eltdev home directory
WORKDIR /home_local/eltdev
RUN mkdir modulefiles

# 8 - Create and edit the file private.lua under modulefiles directory
COPY ./files/private.lua /home_local/eltdev/modulefiles/

# ++ Fixing ACL issues on /var/log (prevents NOMAD / CONSUL to be launched otherwise !)
#USER root
#RUN setfacl -b -R /var/log
#RUN chmod -R g=rwx /var/log/journal
#USER eltdev 


RUN systemctl enable consul
RUN systemctl enable nomad

 
