# Add in the hereafter hash table all mounting point
# form is "Windows host Path" = "Docker mounted path"
$MountingPoints =
@{
    "D:\Projets\EFISOFT\Dev\MORFEO"="/home_local/eltdev/MORFEO"
    "D:\Projets\EFISOFT\Dev\GeneratingTestNew"="/home_local/eltdev/GENERATED"
}

 
$AddHosts =
@{
	"ciihdfshost"="127.0.0.1" 
	"ciielastichost"="127.0.0.1"
	"ciielastichost2"="127.0.0.1" 
	"ciiconfservicehost"="127.0.0.1"
	"ciiconfservicehost2"="127.0.0.1" 
	"ciiarchivehost"="127.0.0.1"
	"ciilogstashhost"="127.0.0.1" 
	"ciilogelastichost"="127.0.0.1"
	"ciitracingelastichost"="127.0.0.1"
} 
$DockerID = -join ((48..57) + (97..122) | Get-Random -Count 8 | % {[char]$_})
 
$WSLIp =  (Get-NetIPConfiguration -InterfaceAlias "*(WSL)*").IPv4Address.IPAddress
$DefaultIp =  (Get-NetIPConfiguration -InterfaceAlias "*(Default Switch)*").IPv4Address.IPAddress
Set-Variable -Name DISPLAY -value $WSLIp":0.0"
 
Set-Variable -Name ROOTPWD -value "root"
  
$Parameters =@()
$Parameters += "-it"
$Parameters += "-e","DISPLAY=$($DISPLAY)"
foreach($Mounting in $MountingPoints.GetEnumerator())
{
    $Parameters+="-v"
    $Parameters+="$($Mounting.Name):$($Mounting.Value)"
}
foreach($AddedHost in $AddHosts.GetEnumerator())
{
    $Parameters+="--add-host=$($AddedHost.Name):$($AddedHost.Value)"
}
#$Parameters += "--user","harmoni"
#$Parameters += "--user","root"
$Parameters += "--user","eltdev"
$Parameters += "-e","ROOT_PASS=$($ROOTPWD)"
#$Parameters += "--expose","22"
#$Parameters += "--expose","8500"
#$Parameters += "--expose","4646"
#$Parameters += "--expose","8000"
$Parameters += "-p","8000:8000"
$Parameters += "-p","4646:4646"
$Parameters += "-p","8500:8500"
$Parameters += "-p","22:22"
$Parameters += "-P"
#$Parameters += "-p","8500:8500"
#$Parameters += "-p","22:22"
$Parameters += "--name","$($DockerID)"
$Parameters +="--privileged"
$Parameters +="--security-opt","seccomp=unconfined"
$Parameters +="efisoft-ifw_5_0:latest"
$Parameters += "bash"
 
Write-Host "DockerID : $($DockerID)"
$Command = "docker exec -it -e DISPLAY=$($DISPLAY) $($DockerID) bash"
Write-Host "To open a new shell "
Write-Host $($Command)
  
#Write-Host "*************************************************"
#Write-Host $($Parameters)
#Write-Host "*************************************************"

docker run $Parameters