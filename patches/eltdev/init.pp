# Rules for eltdev machine.
# lint:ignore:inherits_across_namespaces
class eltdev inherits eltminimal {
# lint:endignore

  # In a separate file (autogenerated if we want) for sake of clarity
  include eltdev::sysrpms

  # Fix contents of file containing the role of the machine
  File_line['elt-role-variable'] {
    line => 'export ELT_ROLE=ELTDEV',
  }

  ### LG : Commented for Docker ###
  #if $::elt_dm == 'yes' or $::elt_dm == 'YES' {
  #  notice('Installing/checking display-manager')
  #  service { 'lightdm':
  #    ensure => 'running',
  #    enable => 'true',
  #  }
  #}
  #else {
  #  notice('Installing/checking no-graph ELTDEV station')
  #  service { 'lightdm':
  #    ensure => 'stopped',
  #    enable => 'false',
  #  }
  #}

  # Graphical login as default and XDMCP enabled
  # file { '/etc/systemd/system/default.target':
  #   ensure => 'link',
  #   target => '/usr/lib/systemd/system/graphical.target',
  # }

  # ELTDEV-782 allow eltdev control Nomad and Consul services
  file { '/etc/polkit-1/rules.d/48-nomad.rules':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/eltdev/48-nomad.rules',
  }

  file { '/etc/polkit-1/rules.d/48-consul.rules':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/eltdev/48-consul.rules',
  }

  Package <| |> -> augeas { 'lightdm':
    context => '/files/etc/lightdm/lightdm.conf',
    changes => [
      'set XDMCPServer/enabled true',
      'set SeatDefaults/xserver-allow-tcp true',
      'set SeatDefaults/user-session mate',
      'set SeatDefaults/greeter-show-manual-login true',
      'set SeatDefaults/greeter-hide-users true',
    ],
  }

  Package <| |> -> file_line { 'lightdm-2':
    path  => '/etc/lightdm/lightdm-gtk-greeter.conf',
    line  => 'background=#0066CC',
    match => '^background=',
  }

  Package <| |> -> file_line { 'lightdm-3':
    path  => '/etc/lightdm/lightdm-gtk-greeter.conf',
    line  => 'show-indicators=~session;~a11y;~power',
    match => 'show-indicators=',
  }

  # ELTDEV-62: Add user eltdev to wireshark group
  Package <| |> -> exec {'/usr/sbin/usermod -a -G wireshark eltdev':
    onlyif  => '/usr/bin/id -Gn eltdev | /bin/egrep -qv wireshark',
    require => User['eltdev'],
  }

  # ELTDEV-150: devops.local certificate management
  # Internal CA
  # DFN CA
  Package <| |> -> file { '/etc/pki/ca-trust/source/anchors/trusted_ca.crt':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/eltdev/trusted_ca.crt',
    notify => Exec['updatecatrust'],
  }
  exec { 'updatecatrust':
    command     => '/usr/bin/update-ca-trust extract',
    refreshonly => 'true',
  }

  ### LG : Commented for Docker ###
  # ELTDEV-313: Xvfb on :2 to run automatic GUI tests without X
  #Package <| |> -> file { '/etc/systemd/system/xvfb.service':
  #  ensure => 'present',
  #  owner  => 'root',
  #  group  => 'root',
  #  mode   => '0755',
  #  source => 'puppet:///modules/eltdev/xvfb.service',
  #  notify => Service['xvfb'],
  #}
  #service { 'xvfb':
  #  ensure => 'running',
  #  enable => 'true',
  #}

  Package <| |> -> file { '/etc/X11/xinit/Xclients':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/eltdev/Xclients',
  }

  group { 'minio-user':
    ensure => 'present',
    gid    => '4644',
  }

  user { 'minio-user':
    ensure  => 'present',
    uid     => '8524',
    gid     => '4644',
    comment => 'MinIO Object Storage',
    require => Group['minio-user'],
  }

  file { '/etc/default/minio.template':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => 'puppet:///modules/eltdev/minio.template',
  }

  # ELTDEV-305: add kernel boot parameters to disable some performance affecting security patches
  # mitigations=off disable all optional CPU mitigations. More details can be found:
  # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/8.2_release_notes/index
  $kernelargs = 'mitigations=off'
  Package <| |> -> exec { 'grubrewrite':
    path     => '/usr/bin:/usr/sbin:/bin',
    provider => shell,
    command  => "grubby --update-kernel=ALL --args=\"${kernelargs}\"",
    # lint:ignore:variables_not_enclosed
    onlyif   => "grubby --info=\"$(grubby --default-kernel)\" | (! grep \"$kernelargs\")",
    # lint:endignore
  }

  # ELTDEV-1045 JDK 8 should be default
  Package <| |> -> exec { 'alternatives --set java java-1.8.0-openjdk.x86_64':
    path   => '/usr/bin:/usr/sbin:/bin',
    unless => 'java -version 2>&1 | grep -q "1\.8\.0"',
  }

  Package <| |> -> exec { 'alternatives --set javac java-1.8.0-openjdk.x86_64':
    path   => '/usr/bin:/usr/sbin:/bin',
    unless => 'javac -version 2>&1 | grep -q "1\.8\.0"',
  }

  Package <| |> -> exec { 'dnf-update':
    path     => '/usr/bin:/usr/sbin:/bin',
    provider => shell,
    command  => '! (dnf -y update -x puppet-elt 2>/dev/null | grep "Skipping packages with conflicts:")',
    unless   => 'dnf check-update -x puppet-elt >/dev/null 2>&1',
  }

  # ELTDEV-1083: Stop services pcscd and pcscd.socket to allow firefox running
  Package <| |> -> service { 'pcscd':
    ensure => 'stopped',
    enable => 'false',
  }
  Package <| |> -> service { 'pcscd.socket':
    ensure => 'stopped',
    enable => 'false',
  }

  ### LG : Commented for Docker ###
  # Enable nix-daemon socket for Nix users
  #Package <| |> -> service { 'nix-daemon.socket':
  #  ensure => 'running',
  #  enable => 'true',
  #}

  # TODO CheckNewDevEnv rename for Fedora 36
  Package <| |> cron { 'CheckNewDevEnv':
    ensure  => 'absent',
  }

  # ELTDEV-1124 Remove firefox default homepage
  Package <| |> -> file_line { 'firefox-default-homepage1':
    path  => '/usr/lib64/firefox/browser/defaults/preferences/firefox-redhat-default-prefs.js',
    match => '^pref\("browser.startup.homepage"',
    line  => '#pref("browser.startup.homepage",            "data:text/plain,browser.startup.homepage=https://start.fedoraproject.org/");',
  }
  Package <| |> -> file_line { 'firefox-default-homepage2':
    path  => '/usr/lib64/firefox/browser/defaults/preferences/firefox-redhat-default-prefs.js',
    match => '^pref\("browser.newtabpage.pinned"',
    line  => '#pref("browser.newtabpage.pinned",           \'[{"url":"https://start.fedoraproject.org/","title":"Fedora Project - Start Page"}]\');',
  }

}
