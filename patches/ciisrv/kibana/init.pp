# kibana function
class kibana {

  notice('kibana: setting up kibana')

  file { '/etc/kibana':
    ensure => 'directory',
    owner  => 'root',
    mode   => '0755',
  }

  file_line { 'kibana':
    ensure  => present,
    path    => '/etc/kibana/kibana.yml',
    line    => 'server.host: "0.0.0.0"',
    match   => '#server.host: "localhost"',
    replace => true,
  }

  # pre-load dashboard into kibana
  # kibana must be running, which needs elasticsearch running.
  # We could start them via cii-services (like we do in schemas/init.pp),
  # but starting them via puppet directives is just as good but simpler.

  service { 'elasticsearch':
    ensure  => 'running',
  	start => 'systemctl start elasticsearch',
 		stop => 'systemctl stop elasticsearch',
 		status => 'systemctl status elasticsearch',
	} -> wait_for { 'elasticsearch-running':
    query              => '/usr/bin/wget -q --spider http://localhost:9200/',
    exit_code          => 0,
    polling_frequency  => 2,
    max_retries        => 15,
  }

  service { 'kibana':
    ensure  => 'running',
   	start => 'systemctl start kibana',
 		stop => 'systemctl stop kibana',
 		status => 'systemctl status kibana',
   require => [ Wait_for['elasticsearch-running'] ],
  } -> wait_for { 'kibana-running':
    # alternatively, use query "wget --server-response" and regex "200 OK"
    query              => '/usr/bin/wget -q --spider http://localhost:5601/status',
    exit_code          => 0,
    polling_frequency  => 2,
    max_retries        => 15,
  }

  file { '/tmp/Dab-EL-ELC-7d-v1.json':
    source => 'puppet:///modules/kibana/Dab-EL-ELC-7d-v1.json',
  }

  # idempotency: if dashboard already loaded, kibana returns a message "object exists", code 200 OK.
  exec { 'load kibana dashboard':
    require  => [ Wait_for['kibana-running'] ],
    command  => "/usr/bin/wget --quiet -O /dev/null \
 --header=\"kbn-xsrf: true\" --header=\"Content-Type: application/json\" \
 --post-file=/tmp/Dab-EL-ELC-7d-v1.json \
   \"localhost:5601/api/kibana/dashboards/import\" ",
  }

}