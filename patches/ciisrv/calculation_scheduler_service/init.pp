# calculation_scheduler_service, calculation scheduler for oldb
class calculation_scheduler_service {

  file { '/etc/systemd/system/calculation-scheduler-service.service':
    ensure => 'present',
    source => '/elt/ciisrv/etc/systemd/system/calculation-scheduler-service.service',
    notify => [
      Service['calculation-scheduler-service'],
    ],
  }

  service { 'calculation-scheduler-service':
  	start => 'systemctl start calculation-scheduler-service',
 		stop => 'systemctl stop calculation-scheduler-service',
 		status => 'systemctl status calculation-scheduler-service',
   enable  => true,
  }
}