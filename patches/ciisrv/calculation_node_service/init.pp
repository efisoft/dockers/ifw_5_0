# calculation_node_service, calculation node for oldb
class calculation_node_service {

  package { 'octave':
    ensure  => installed,
  }

  file { '/etc/systemd/system/calculation-node-service.service':
    ensure => 'present',
    source => '/elt/ciisrv/etc/systemd/system/calculation-node-service.service',
    notify => [
      Service['calculation-node-service'],
    ],
  }

  service { 'calculation-node-service':
 		start => 'systemctl start calculation-node-service',
 		stop => 'systemctl stop calculation-node-service',
 		status => 'systemctl status calculation-node-service',
    enable  => true,
  }
}