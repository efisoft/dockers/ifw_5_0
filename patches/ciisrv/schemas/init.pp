# schemas
class schemas {

  $ciiconfigfile = '/etc/ciisrv-config-version'
  $ciioldbfile = '/etc/ciisrv-oldb-version'
  $ciitelemetryfile = '/etc/ciisrv-telemetry-version'
  $ciialarmfile = '/etc/ciisrv-alarm-version'

  notice('schemas: populating elasticsearch')

  exec { '/elt/ciisrv/bin/cii-services start elasticsearch':
    unless => '/usr/bin/wget -q --spider http://localhost:9200',
  } -> wait_for { 'elasticsearch':
    query             => '/usr/bin/wget -qO- http://localhost:9200',
    regex             => 'elasticsearch',
    polling_frequency => 2,  # Wait up to 30 seconds (15 * 2 seconds).
    max_retries       => 15,
  }

  exec { 'schemas: init config':
    command  => '/elt/ciisrv/bin/config-initEs.sh -s',
    provider => 'shell',
    unless   => "/usr/bin/test -f ${ciiconfigfile}",
    user     => 'eltdev',
  } -> exec { "schemas: save cii schema version in ${ciiconfigfile}":
    command  => "rpm -q --queryformat '%{VERSION}\n' elt-ciisrv-srv-config > ${ciiconfigfile}",
    unless   => "/usr/bin/test -f ${ciiconfigfile}",
    provider => 'shell',
  } -> service { 'srv-config':
 		start => 'systemctl start srv-config',
 		stop => 'systemctl stop srv-config',
 		status => 'systemctl status srv-config',
    ensure  => 'running',
  } -> wait_for { 'srv-config':
    query             => "/bin/sh -l -c \"/elt/ciisrv/bin/client-api-util-ciismitool.sh service:config host:${::server}\"",
    regex             => 'UpTime',
    polling_frequency => 4,  # Wait up to 60 seconds (15 * 4 seconds).
    max_retries       => 15,
  }

  exec { 'schemas: init oldb':
    command  => '/bin/sh -l -c /elt/ciisrv/bin/oldb-initEs',
    provider => 'shell',
    unless   => "/usr/bin/test -f ${ciioldbfile}",
    user     => 'eltdev',
  } -> exec { "schemas: save cii schema version in ${ciioldbfile}":
    command  => "rpm -q --queryformat '%{VERSION}\n' elt-ciisrv-oldb-client > ${ciioldbfile}",
    unless   => "/usr/bin/test -f ${ciioldbfile}",
    provider => 'shell',
  }

  exec { 'schemas: init telemetry':
    command => '/bin/sh -l -c /elt/ciisrv/bin/telemetry-initEs',
    onlyif  => '/usr/bin/test -f /elt/ciisrv/bin/telemetry-initEs',
    unless  => "/usr/bin/test -f ${ciitelemetryfile}",
    user    => 'eltdev',
  } -> exec { "schemas: save cii schema version in ${ciitelemetryfile}":
    command  => "rpm -q --queryformat '%{VERSION}\n' elt-ciisrv-srv-telemetry > ${ciitelemetryfile}",
    unless   => "/usr/bin/test -f ${ciitelemetryfile}",
    provider => 'shell',
  }

  # msc: this init script comes from module alarm-config-gui.
  # we plan to retire that gui, and then we can skip this step.

  exec { 'schemas: init alarms':
    command => '/bin/sh -l -c /elt/ciisrv/bin/alarmCfg-initEs.sh',
    onlyif  => '/usr/bin/test -f /elt/ciisrv/bin/alarmCfg-initEs.sh',
    unless  => "/usr/bin/test -f ${ciialarmfile}",
    user    => 'eltdev',
  } -> exec { "schemas: save cii schema version in ${ciialarmfile}":
    command  => "rpm -q --queryformat '%{VERSION}\n' elt-ciisrv-alarm-config-gui > ${ciialarmfile}",
    unless   => "/usr/bin/test -f ${ciialarmfile}",
    provider => 'shell',
  }
}
