# elasticsearch
class elasticsearch {

  include ::systemd::daemon_reload

  package { 'elasticsearch-6.6.2-1':
    ensure  => installed,
  }

  service { 'elasticsearch':
    require => Class['systemd::daemon_reload'],
 		start => 'systemctl start elasticsearch',
 		stop => 'systemctl stop elasticsearch',
 		status => 'systemctl status elasticsearch',
    enable  => true,
  }

  file_line { 'configure Xms':
    path    => '/etc/elasticsearch/jvm.options',
    line    => '-Xms64m',
    match   => '^-Xms.*$',
    replace => true,
  }

  file_line { 'configure Xmx':
    path    => '/etc/elasticsearch/jvm.options',
    line    => '-Xmx256m',
    match   => '^-Xmx.*$',
    replace => true,
  }

  include elasticsearch_singlenode
}