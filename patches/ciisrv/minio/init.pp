# minio
class minio {

  notice('minio: setting up minio')

  include oldb

  $miniobucketsroot='/var/tmp/minio'
  $configbucketname='config'
  $oldbbucketname=$oldb::largestoragebucketname

  file { "$miniobucketsroot":
    ensure => directory,
    owner  => 'minio-user',
    group  => 'minio-user',
    mode   => '0755',
  }~> file { "$miniobucketsroot/$configbucketname":
    ensure => directory,
    owner  => 'minio-user',
    group  => 'minio-user',
    mode   => '0755',
  }~> file { "$miniobucketsroot/$oldbbucketname":
    ensure => directory,
    owner  => 'minio-user',
    group  => 'minio-user',
    mode   => '0755',
  }

  file { '/etc/default/minio':
    ensure  => file,
    content => epp('minio/minio.epp', {
      'minioSecretKey' => $oldb::largestoragesecretkey,
      'minioAccessKey' => $oldb::largestorageaccesskey,
      'minioVolumes'   => $miniobucketsroot,
      'minioPort'      => $oldb::largestorageport})
  }
# The username on DevEnv4/Fedora is 'minio-user' (it was 'minio' on DevEnv3/CentOS)
#  file_line { 'minio_1':
#    ensure  => present,
#    path    => '/etc/systemd/system/minio.service',
#    line    => 'User=minio',
#    match   => 'User=minio-user',
#    notify  => Service['minio'],
#    replace => true,
#  }~> file_line { 'minio_2':
#    ensure  => present,
#    path    => '/etc/systemd/system/minio.service',
#    line    => 'Group=minio',
#    match   => 'Group=minio-user',
#    notify  => Service['minio'],
#    replace => true,
#  }
  service { 'minio':
    ensure => 'running',
    enable => true,
   	start => 'systemctl start minio',
 		stop => 'systemctl stop minio',
 		status => 'systemctl status minio',
  }
}