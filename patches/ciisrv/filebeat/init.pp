# function filebeat
class filebeat {

  notice('filebeat: setting up filebeat settings')
  file { '/etc/filebeat/modules.d/elasticsearch.yml':
    ensure => 'absent',
    backup => '.disabled',
  }

  file_line { 'filebeat.yml_1':
    ensure  => present,
    path    => '/etc/filebeat/filebeat.yml',
    line    => 'output.logstash:',
    match   => '#output.logstash:',
    replace => true,
  }

  file_line { 'filebeat.yml_2':
    ensure  => present,
    path    => '/etc/filebeat/filebeat.yml',
    line    => '  hosts: ["ciilogstashhost:5044"]',
    match   => '  #hosts: \\["localhost:5044"\\]',
    replace => true,
  }

  file_line { 'filebeat.yml_3':
    ensure => present,
    path   => '/etc/filebeat/filebeat.yml',
    line   => '    - /elt/ciisrv/logsink/*.log',
    after  => '    - /var/log/\\*.log',
  }

  file_line { 'filebeat.yml_4':
    ensure  => present,
    path    => '/etc/filebeat/filebeat.yml',
    line    => '    - /var/log/elt/*.log',
    match   => '    - /var/log/\\*.log',
    replace => true,
  }

  file_line { 'filebeat.yml_5':
    ensure  => present,
    path    => '/etc/filebeat/filebeat.yml',
    line    => '#output.elasticsearch:',
    match   => 'output.elasticsearch:',
    replace => true,
  }

  file_line { 'filebeat.yml_6':
    ensure  => present,
    path    => '/etc/filebeat/filebeat.yml',
    line    => '  #hosts: ["localhost:9200"]',
    match   => '  hosts: ["localhost:9200"]',
    replace => true,
  }

  file_line { 'filebeat.yml_7':
    ensure  => present,
    path    => '/etc/filebeat/filebeat.yml',
    line    => '  enabled: true',
    match   => '  enabled: false',
    replace => true,
  }

  service { 'filebeat':
    ensure  => running,
 		start => 'systemctl start filebeat',
 		stop => 'systemctl stop filebeat',
 		status => 'systemctl status filebeat',
    enable  => true,
  }
}