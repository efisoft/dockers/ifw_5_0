# Rules for all ELT machines.
class eltgeneral {

  # A few users and groups needed by default in ELT
  user { 'eltmgr':
    ensure     => 'present',
    uid        => '8267',
    gid        => '39542',
    home       => '/home_local/eltmgr',
    managehome => true,
  }
  # Change pass only if not present, default password => "HOuJxJbENTBs2" (pass4u)
  exec {  "/usr/sbin/usermod -p 'HOuJxJbENTBs2' eltmgr":
    onlyif  => '/bin/egrep -q "^eltmgr:[*!]" /etc/shadow',
    require => User['eltmgr'],
  }
  # ELTDEV-230 Add user eltdev to group video
  user { 'eltdev':
    ensure     => 'present',
    uid        => '6173',
    gid        => '39542',
    groups     => [ 'video', 'systemd-journal' ],
    home       => '/home_local/eltdev',
    managehome => true,
  }

  # Change pass only if not present, default password => "x/PCPZWO2m8so" (2Garch1ng)
  exec {  "/usr/sbin/usermod -p 'x/PCPZWO2m8so' eltdev":
    onlyif  => '/bin/egrep -q "^eltdev:[*!]" /etc/shadow',
    require => User['eltdev'],
  }

  group { 'elt':
    ensure => 'present',
    gid    => '39542',
  }

  # Disable default firewalls
  service { 'firewalld':
    ensure => 'stopped',
    enable => 'false',
  }

  service { 'systemd-oomd':
    ensure => 'stopped',
    enable => 'mask',
  }

  # ELTDEV-131 Disable default abrtd
  service { 'abrtd':
    ensure => 'stopped',
    enable => 'false',
  }

  # Disable SELinux
  # if ! ($facts['virtual'] in ['docker', 'podman', 'oci']) {
  #  augeas { 'selinux':
  #    context => '/files/etc/sysconfig/selinux',
  #    changes => [
  #      'set SELINUX disabled',
  #    ],
  #    notify  => Exec['selinux_immediate_stop'],
  #  }

   # exec { 'selinux_immediate_stop':
   #   command     => '/usr/sbin/setenforce 0',
   #   refreshonly => 'true',
   # }
  #}


  # Disable default Fedora repositories
  yumrepo { 'fedora':
    enabled => 0,
  }

  yumrepo { 'fedora-modular':
    enabled => 0,
  }

  yumrepo { 'fedora-updates':
    name    => 'updates',
    descr   => 'updates',
    enabled => 0,
  }

  yumrepo { 'fedora-updates-modular':
    descr   => 'updates-modular',
    name    => 'updates-modular',
    enabled => 0,
  }

  yumrepo { 'fedora-cisco-openh264':
    enabled => 0,
  }

  # Enable ESO mirror
  yumrepo { 'eso-fedora':
    descr    => 'eso-fedora',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/releases/$releasever/Everything/$basearch/os/',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 1,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-debuginfo':
    descr    => 'eso-fedora-debuginfo',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/releases/$releasever/Everything/$basearch/debug/tree',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-source':
    descr    => 'eso-fedora-source',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/releases/$releasever/Everything/source/tree',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-modular':
    descr    => 'eso-fedora-modular',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/releases/$releasever/Modular/$basearch/os/',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 1,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-modular-debuginfo':
    descr    => 'eso-fedora-modular-debuginfo',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/releases/$releasever/Modular/$basearch/debug/tree',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-modular-source':
    descr    => 'eso-fedora-modular-source',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/updates/$releasever/Modular/source/tree',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-updates':
    descr    => 'eso-fedora-updates',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/updates/$releasever/Everything/$basearch/',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 1,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-updates-debuginfo':
    descr    => 'eso-fedora-updates-debuginfo',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/updates/$releasever/Everything/$basearch/debug',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-updates-source':
    descr    => 'eso-fedora-updates-source',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/updates/$releasever/Everything/source/tree',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-updates-modular':
    descr    => 'eso-fedora-updates-modular',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/updates/$releasever/Modular/$basearch/',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 1,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-updates-modular-debuginfo':
    descr    => 'eso-fedora-updates-modular-debuginfo',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/updates/$releasever/Modular/$basearch/debug',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  yumrepo { 'eso-fedora-updates-modular-source':
    descr    => 'eso-fedora-updates-modular-source',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/updates/$releasever/Modular/source/tree',
    gpgkey   => 'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch',
    enabled  => 0,
    gpgcheck => 1,
  }

  # ESO ELT Repositories
  yumrepo { 'eso-elt':
    descr    => 'eso-elt',
    baseurl  => "https://ftp.eso.org/pub/elt/repos/fedora/eso/\$releasever/elt/\$basearch/",
    enabled  => 1,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-release':
    descr    => 'eso-elt-release',
    baseurl  => "https://ftp.eso.org/pub/elt/repos/fedora/eso/\$releasever/elt-${::elt_repo}/\$basearch/",
    enabled  => 1,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-release-debuginfo':
    descr    => 'eso-elt-release-debuginfo',
    baseurl  => "https://ftp.eso.org/pub/elt/repos/fedora/eso/\$releasever/elt-${::elt_repo}/\$basearch/debug",
    enabled  => 0,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-release-source':
    descr    => 'eso-elt-release-source',
    baseurl  => "https://ftp.eso.org/pub/elt/repos/fedora/eso/\$releasever/elt-${::elt_repo}/source",
    enabled  => 0,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-beta':
    descr           => 'eso-elt-beta',
    baseurl         => 'https://ftp.eso.org/pub/elt/repos/fedora/eso/$releasever/elt-beta/$basearch',
    enabled         => 0,
    gpgcheck        => 0,
    metadata_expire => 60,
  }

  yumrepo { 'eso-elt-beta-debuginfo':
    descr    => 'eso-elt-debuginfo',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/eso/$releasever/elt-beta/$basearch/debug',
    enabled  => 0,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-beta-source':
    descr    => 'eso-elt-source',
    baseurl  => 'https://ftp.eso.org/pub/elt/repos/fedora/eso/$releasever/elt-beta/source',
    enabled  => 0,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-local':
    descr    => 'eso-elt-local',
    baseurl  => 'http://fenice.hq.eso.org/REPO/Local-Fedora-34/$basearch',
    enabled  => 0,
    gpgcheck => 0,
  }

  # ELTDEV-1102
  yumrepo { 'eso-elt-projects':
    descr           => 'eso-elt-projects',
    baseurl         => "https://ftp.eso.org/pub/elt/repos/fedora/eso/\$releasever/elt-projects-${::elt_repo}/\$basearch/",
    enabled         => 1,
    gpgcheck        => 0,
    metadata_expire => 60,
  }

  yumrepo { 'eso-elt-projects-debuginfo':
    descr    => 'eso-elt-projects-debuginfo',
    baseurl  => "https://ftp.eso.org/pub/elt/repos/fedora/eso/\$releasever/elt-projects-${::elt_repo}/\$basearch/debug",
    enabled  => 0,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-projects-source':
    descr    => 'eso-elt-projects-source',
    baseurl  => "https://ftp.eso.org/pub/elt/repos/fedora/eso/\$releasever/elt-projects-${::elt_repo}/source",
    enabled  => 0,
    gpgcheck => 0,
  }

  yumrepo { 'eso-elt-users':
    descr           => 'eso-elt-users',
    baseurl         => "http://eltdnf.hq.eso.org/fedora/eso/\$releasever/elt-users/\$basearch/",
    enabled         => 0,
    gpgcheck        => 0,
    metadata_expire => 60,
  }

  # Ensure local time zone is UTC (ELTMGR-191)
  file { '/etc/localtime':
    ensure => 'link',
    target => '../usr/share/zoneinfo/UTC',
  }

  # ELTDEV-112 Sudo rules for eltdev account
  file { '/etc/sudoers.d/eltdev':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0640',
    source => 'puppet:///modules/eltgeneral/eltdev-sudoers',
  }

  # File containing the role of the machine
  file { '/etc/profile.d/eltdev.sh':
    ensure  => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    replace => 'false',
  #  source  => 'puppet:///modules/eltgeneral/eltdev.sh',
  }

  file_line { 'elt-role-variable':
    path    => '/etc/profile.d/eltdev.sh',
    line    => 'export ELT_ROLE=MINIMAL',
    match   => 'export ELT_ROLE=',
    require => File['/etc/profile.d/eltdev.sh'],
  }

  file_line { 'elt-dm-variable':
    path    => '/etc/profile.d/eltdev.sh',
    line    => "export ELT_DM=${::elt_dm}",
    match   => 'export ELT_DM=',
    require => File['/etc/profile.d/eltdev.sh'],
  }

  file_line { 'elt-release':
    path    => '/etc/profile.d/eltdev.sh',
    line    => "export ELT_RELEASE=${::elt_release}",
    match   => 'export ELT_RELEASE=',
    require => File['/etc/profile.d/eltdev.sh'],
  }

  # Doxypypy support script
  file { '/usr/local/bin/py_filter':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/eltgeneral/py_filter',
  }

  file { '/root/bin':
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }


  # ELTDEV-771. Script to upgrade a host to Beta release
  file { '/root/bin/update-puppet-beta':
    ensure => 'file',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/eltgeneral/update-puppet-beta',
  }

  # ELTDEV-49
  if ! ($facts['virtual'] in ['docker', 'podman', 'oci']) {
    file { '/root/bin/eltdev-status.sh':
      ensure => 'file',
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
      source => 'puppet:///modules/eltgeneral/eltdev-status.sh',
    }

    file { '/usr/lib/systemd/system/eltdev-status.service':
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      source  => 'puppet:///modules/eltgeneral/eltdev-status.service',
      require => File['/root/bin/eltdev-status.sh'],
    }

    file { '/usr/lib/systemd/system/eltdev-status.timer':
      ensure  => 'file',
      owner   => 'root',
      group   => 'root',
      mode    => '0755',
      source  => 'puppet:///modules/eltgeneral/eltdev-status.timer',
      require => File['/usr/lib/systemd/system/eltdev-status.service'],
    }

    service { 'eltdev-status.timer':
      ensure  => true,
      enable  => true,
      require => File['/usr/lib/systemd/system/eltdev-status.timer'],
    }

    file { '/etc/NetworkManager/dispatcher.d/ifup-lo':
      ensure => 'present',
      owner  => 'root',
      group  => 'root',
      mode   => '0755',
      source => 'puppet:///modules/eltgeneral/ifup-lo',
    }


    file_line { 'eltdev-156-1':
      path => '/etc/sysctl.conf',
      line => 'net.ipv4.neigh.default.gc_thresh1 = 4096',
    }

    file_line { 'eltdev-156-2':
      path => '/etc/sysctl.conf',
      line => 'net.ipv4.neigh.default.gc_thresh2 = 8192',
    }

    file_line { 'eltdev-156-3':
      path => '/etc/sysctl.conf',
      line => 'net.ipv4.neigh.default.gc_thresh3 = 8192',
    }

    file_line { 'eltdev-156-4':
      path => '/etc/sysctl.conf',
      line => 'net.ipv4.neigh.default.gc_stale_time = 86400',
    }
  }

  # ELTDEV-95: create a scratch area under /home_local
  # It is not always possible as in NIS servers /home is automounted.
  # It is better to check if /scratch already exists as a soft link.
  # otherwise create /home_local/scratch and link /scratch -> /home_local/scratch
  exec { '/usr/bin/mkdir -n /home_local/scratch ; /usr/bin/chmod 1777 /home_local/scratch; /usr/bin/ln -s /home_local/scratch /':
          onlyif => '/usr/bin/test ! -h /scratch',
  }

  # Default wafcache directory (and parent) for Jenkins
  Package <| |> -> file { [ '/var/cache/wafcache', '/var/cache/wafcache/global' ]:
    ensure => 'directory',
    owner  => '6173',  # eltdev
    group  => '39542', # elt
    mode   => '0755',
  }

  # ELTDEV-1185, modify hosts line in /etc/nsswitch.conf as in CentOS-8
  file_line { '/etc/nsswitch.conf':
    path  => '/etc/nsswitch.conf',
    line  => 'hosts:      files dns myhostname resolve [!UNAVAIL=return]',
    match => '^hosts:'
  }
}
