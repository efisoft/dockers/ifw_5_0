# List of RPMs for a minimal installation
class eltminimal::sysrpms {

  # Just one pre-existing RPM for proper execution
  $myrpmlist = [
    'coreutils',
  ]

  if $::fast_yum_install == 'YES' {
    # Puppet is extremely slow on single RPM, this is a shortcut just for first installation
    $pkglist = join($myrpmlist,' ')
    exec { 'install_minimal':
      command => "/usr/bin/dnf --setopt=install_weak_deps=False -y install ${pkglist}",
      timeout => 0,
    }
  } else {
    package { $myrpmlist:
      ensure => 'installed',
    }
  }
}
