# IFW-DOCKER

## Introduction
ESO provides a virtual machine as well as manual instructions to create it the following way:

- Using the visual installer
- Download some dependencies from ESO's ftp
- Running puppet

The virtual machine is ready-to-use and should be portable which is great. However, using a virtual machine
as a day to day development environment can be painful. Examples of drawbacks:
- It's stateful. Which means that you can easily change its configuration and break your build system.
- You run an entire guest OS which makes it fairly heavy and slow. (Load in ~1+ minute)
- Running multiple instances means running multiple guest OSs so we can't do this without a major perf impact.

This repo proposes a docker version of the IFW workstation which bring the following benefits:
- Starts instantly (we're talking milliseconds)
- Native volume mounting (so you can easily work on your host system)
- Easy deployment to a Continuous Integration server using a container registry. (Like on this Gitlab)
- Lightweight and more performance than VMs.

## Building
```
 docker build --add-host=ciihdfshost:127.0.0.1 --add-host=ciielastichost:127.0.0.1 --add-host=ciielastichost2:127.0.0.1 --add-host=ciiconfservicehost:127.0.0.1 --add-host=ciiconfservicehost2:127.0.0.1 --add-host=ciiarchivehost:127.0.0.1 --add-host=ciilogstashhost:127.0.0.1 --add-host=ciilogelastichost:127.0.0.1 --add-host=ciitracingelastichost:127.0.0.1 -t efisoft-ifw_5_0 .
```

N.B. : some time the build can fails on hlccOldbloader installation stage.
Run build again and everything should works fine.

## Executing

The following command can be used to execute th docker.
N.B. : its assumed that the XServer IP to be used by docker is 172.23.96.1
It could be retreived using ipconfig / ifconfig command
This is mandatory for all GUI associated program to be executed
```
docker run --add-host=ciihdfshost:127.0.0.1 --add-host=ciielastichost:127.0.0.1 --add-host=ciielastichost2:127.0.0.1 --add-host=ciiconfservicehost:127.0.0.1 --add-host=ciiconfservicehost2:127.0.0.1 --add-host=ciiarchivehost:127.0.0.1 --add-host=ciilogstashhost:127.0.0.1 --add-host=ciilogelastichost:127.0.0.1 --add-host=ciitracingelastichost:127.0.0.1  -it -e DISPLAY=172.23.96.1:0.0 --privileged efisoft-ifw_5_0 bash
```
